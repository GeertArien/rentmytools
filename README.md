# OPLOSSING PROJECT WEBSCRIPTEN 2 (2014)

## Requirements

- [Composer](http://getcomposer.org/)
- PHP 5.4

## Installation

- Get the source and install the dependencies

	$ composer install

- Create the database (in the project root)

	rentmytools.sql

## Running the project

### Using PHP 5.4

- Open a shell, navigate to the project root and run `php -S localhost:8080 -t web web/index.php` to start a PHP web server
- Open `http://localhost:8080/` in your browser

### Using your favorite webserver

- Create a virtualhost pointing to the web folder
- Make sure you've enabled rewriting
	- See [http://silex.sensiolabs.org/doc/web_servers.html](http://silex.sensiolabs.org/doc/web_servers.html) for more info
	- A `.htaccess` for use with Apache is already included


#### CSS framework: 

- BOOTSTRAP v.3.1.1 (http://getbootstrap.com/)
- Spacelab theme (http://bootswatch.com/spacelab/)

#### Registered users with password:

	sticman@telenet.be   		:	Azerty123
	dieterjanssens@telenet.be	: 	Qwerty123
	tinaverschueren@telenet.be	: 	Toledo
	poldriessens@telenet.be	: 	Metropool99

#### The website can be reached at the following url:

`http://rentmytools.geertarien.ikdoeict.be/`
