<?php
/**
 * Utility class
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Utility;

class Helper {

    // get all dates between a start and end date
    public static function DateInterval($start, $end) {
        $datePeriod = new \DatePeriod(
            new \DateTime($start),
            new \DateInterval('P1D'),
            new \DateTime($end)
        );

        // formate the array to mysql compatible format
        $period = array();
        foreach( $datePeriod as $date) {
            $period[] = $date->format('Y-m-d');
        }

        // add end date to the array
        $period[] = $end;

        return $period;
    }



    // check if an array of dates contains a given date
    public static function ContainsPeriod($newPeriod, $period) {
        $contains = true;
        foreach ($newPeriod as $date) {
            if (!in_array($date, $period)) {
                $contains = false;
                break;
            }
        }
        return $contains;
    }



    // exclude time intervals from a given time interval
    public static function excludeIntervals($intervals, $period) {
        $intervalPeriods = array();

        // convert each time interval to an array of dates
        foreach ($intervals as $interval) {
            $intervalPeriods[] = Helper::DateInterval($interval['start_date'], $interval['end_date']);
        }

        // remove all dates that are in both date arrays
        foreach ($intervalPeriods as $intervalPeriod) {
            foreach ($intervalPeriod as $date)
            if (in_array($date, $period)) {
                unset($period[array_search($date, $period)]);
            }
        }
        return array_values($period);
    }



    // create date ranges (start-end date) from a given array with dates
    public static function createRanges($period) {
        $ranges = array();
        $ranges[0]['start_date'] = $period[0];
        $counter = 0;

        for ($i = 1; $i < count($period); $i++) {
            $date = new \DateTime($period[$i - 1]);
            $date->add(new \DateInterval('P1D'));
            if ($date->format('Y-m-d') != $period[$i] || $i + 1 == count($period)) {
                $ranges[$counter]['end_date'] = $period[$i - 1];
                if ($i + 1 != count($period)) {
                    $counter++;
                    $ranges[$counter]['start_date'] = $period[$i];
                }
            }
        }
        return $ranges;
    }
}


