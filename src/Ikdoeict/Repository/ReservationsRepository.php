<?php
/**
 * Repository for reservations database
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Repository;

class ReservationsRepository extends \Knp\Repository {

    public function getTableName() {
        return 'reservations';
    }

    public function findForOwner($reservationId, $ownerId) {
        return $this->db->fetchColumn('SELECT * FROM reservations INNER JOIN tools ON tools.id = tool_id'
            . ' WHERE reservations.id = ? AND owner_id = ?', array($reservationId, $ownerId));
    }

    public function findForDecline($reservationId) {
        return $this->db->fetchAssoc('SELECT tools.id AS tool_id, tools.name AS tool_name, reservations.start_date,'
            . ' reservations.end_date, categories.name AS tool_categorie, users.firstname, users.email FROM reservations'
            . ' INNER JOIN tools ON tools.id = tool_id'
            . ' INNER JOIN users ON users.id = rentee_id'
            . ' INNER JOIN tools_has_categories ON tools_id = tools.id'
            . ' INNER JOIN categories ON categories.id = categories_id'
            . ' WHERE reservations.id = ?', array($reservationId));
    }

    public function findForAccept($reservationId) {
        return $this->db->fetchAssoc('SELECT tools.id AS tool_id, tools.name AS tool_name, reservations.start_date,'
            . ' reservations.end_date, users.firstname AS rentee_firstname, users.lastname AS rentee_lastname, '
            . ' users.email AS rentee_email, users.address AS rentee_address, cities.name AS rentee_city,'
            . ' cities.code AS rentee_code FROM reservations'
            . ' INNER JOIN tools ON tools.id = tool_id'
            . ' INNER JOIN users ON users.id = rentee_id'
            . ' INNER JOIN cities ON cities.id = city'
            . ' WHERE reservations.id = ?', array($reservationId));
    }

    public function findByToolId($toolId) {
        return $this->db->fetchAll('SELECT start_date, end_date FROM reservations WHERE tool_id = ?'
            . ' AND status != "declined"', array($toolId));
    }

    public function findForUser($userId) {
        return $this->db->fetchAll('SELECT DISTINCT(reservations.id), tool_id, owner_id, reservations.start_date, rentee_id,'
            . ' reservations.end_date, status, users.firstname AS rentee_firstname, users.lastname AS rentee_lastname,'
            . ' tools.name AS tool_name, message FROM reservations'
            . ' INNER JOIN users ON users.id = reservations.rentee_id'
            . ' INNER JOIN tools ON tools.id = tool_id'
            . ' WHERE owner_id = ? AND status != "declined"'
            . ' ORDER BY status DESC, reservations.start_date ASC', array($userId));
    }

}