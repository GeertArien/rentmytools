<?php
/**
 * Repository for tools database
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Repository;

class ToolsRepository extends \Knp\Repository {



    public function getTableName() {
        return 'tools';
    }



    public function getCategories() {
        $categories = $this->db->fetchAll('SELECT id, name FROM categories ORDER BY name');
        $categoriesArray = [];
        for ($i = 0; $i < count($categories); $i++) {
            $categoriesArray[$categories[$i]['id']] = $categories[$i]['name'];
        }
        return $categoriesArray;
    }



    public function insert(array $data) {
        $categories = $data['categories'];
        unset($data['categories']);

        // insert tool
        $this->db->insert($this->getTableName(), $data);
        $toolCategorie['tools_id'] = $this->db->lastInsertId();

        // insert categories for tool
        foreach($categories as $categorie) {
            $toolCategorie['categories_id'] = $categorie;
            $this->db->insert('tools_has_categories', $toolCategorie);
        }
        return $toolCategorie['tools_id'];
    }



    public function update(array $data, array $identifier) {
        $categories = $data['categories'];
        unset($data['categories']);

        // update tool
        $this->db->update($this->getTableName(), $data, $identifier);

        // delete all categories for tool
        $this->db->delete('tools_has_categories', array('tools_id' => $identifier['id']));

        // insert all new categories for tool
        foreach($categories as $categorie) {
            $toolCategorie['tools_id'] = $identifier['id'];
            $toolCategorie['categories_id'] = $categorie;
            $this->db->insert('tools_has_categories', $toolCategorie);
        }
    }



    public function delete(array $identifier) {
        // delete all reservations for this tool
        $this->db->delete('reservations', array('tool_id' => $identifier['id']));
        // delete all categories for this tool
        $this->db->delete('tools_has_categories', array('tools_id' => $identifier['id']));
        // delete tool
        $this->db->delete($this->getTableName(), $identifier);
    }



    public function getToolsByOwner($ownerId) {
        $tools = $this->db->fetchAll('SELECT * FROM tools WHERE owner_id = ?', array($ownerId));
        // find all categories for each tool
        $this->findCategories($tools);
        return $tools;
    }



    public function findForOwner($toolId, $ownerId) {
        $tool = $this->db->fetchAssoc('SELECT * FROM tools WHERE id = ? AND owner_id = ?', array($toolId, $ownerId));
        // find all categories for tool
        if ($tool) {
            $categories = $this->db->fetchAll('SELECT categories_id FROM tools_has_categories
              WHERE tools_id = ?', array($toolId));

            for ($i = 0; $i < count($categories); $i++) {
                $tool['categories'][$i] = $categories[$i]['categories_id'];
            }
        }
        return $tool;
    }



    public function find($id) {
        $tool = $this->db->fetchAssoc('SELECT tools.id, tools.name,description, price_day, owner_id, start_date, end_date,
            cities.name AS city, code, provinces.name AS province, users.firstname, users.lastname  FROM tools
            INNER JOIN users ON owner_id = users.id
            INNER JOIN cities ON city = cities.id
            INNER JOIN provinces ON province = provinces.id
            WHERE tools.id = ?', array((int) $id));

        // find all categories for tool
        $categories = $this->db->fetchAll('SELECT name FROM categories
                INNER JOIN tools_has_categories ON id = categories_id
                WHERE tools_id = ?', array($tool['id']));
        for ($i = 0; $i < count($categories); $i++) {
            $tool['categories'][$i] = $categories[$i]['name'];
        }

        return $tool;
    }



    public function findSimilar($tool) {
        $categories = '';
        // turn categories array in to a string
        foreach ($tool['categories'] as $categorie) {
            $categories .= $this->db->quote($categorie, \PDO::PARAM_STR);
            if ($categorie !== end($tool['categories'])) {
                $categories .= ',';
            }
        }

        // find maximum 3 similar tools
        $tools = $this->db->fetchAll('SELECT DISTINCT(tools.id), tools.name, price_day, cities.name AS city, code, provinces.name AS province FROM tools'
            . ' INNER JOIN users ON users.id = tools.owner_id'
            . ' INNER JOIN cities ON cities.id = users.city'
            . ' INNER JOIN provinces ON provinces.id = cities.province'
            . ' LEFT JOIN tools_has_categories ON tools.id = tools_id'
            . ' INNER JOIN categories ON categories.id = categories_id'
            . ' WHERE categories.name in ('. $categories . ')'
            . ' AND tools.id != ?'
            . ' ORDER BY rand()'
            . ' LIMIT 3', array($tool['id']));

        // didn't find 3 tools, add random tools
        if (count($tools) < 3) {
            $exclude = $this->db->quote($categorie, \PDO::PARAM_STR);
            foreach ($tools as $tool) {
                $exclude .= ',' . $this->db->quote($tool['id'], \PDO::PARAM_STR);
            }
            $number = 3 - count($tools);
            $extraTools = $this->db->fetchAll('SELECT DISTINCT(tools.id), tools.name, price_day, cities.name AS city, code, provinces.name AS province FROM tools'
                . ' INNER JOIN users ON users.id = tools.owner_id'
                . ' INNER JOIN cities ON cities.id = users.city'
                . ' INNER JOIN provinces ON provinces.id = cities.province'
                . ' AND tools.id not in (' . $exclude . ')'
                . ' ORDER BY rand()'
                . ' LIMIT ' . (3 - count($tools)));
            $tools = array_merge($tools, $extraTools);
        }
        return $tools;
    }



    public function countAllFiltered($filter) {
        $extraJoins = '';
        $extraWhere = '';
        // adjust query with extra statements
        $this->adjustQuery($filter, $extraJoins, $extraWhere);
        return $this->db->fetchColumn('SELECT COUNT(DISTINCT(tools.id)) FROM tools' . $extraJoins . ' WHERE 1' . $extraWhere);
    }



    public function findAllFiltered($filter, $curpage = 1, $numItemsPerPage = 10) {
        $extraJoins = '';
        $extraWhere = '';

        // adjust query with extra statements
        $this->adjustQuery($filter, $extraJoins, $extraWhere);

        // add needed statements if they weren't included by the adjust query
        if (strpos($extraJoins, 'JOIN users') === false) {
            $extraJoins .= ' INNER JOIN users ON owner_id = users.id';
        }
        if (strpos($extraJoins, 'JOIN cities') === false) {
            $extraJoins .= ' INNER JOIN cities ON city = cities.id';
        }

        $tools = $this->db->fetchAll('SELECT DISTINCT(tools.id), tools.name, price_day, cities.name AS city, code,'
            . ' start_date, end_date, provinces.name AS province FROM tools'
            . $extraJoins . ' INNER JOIN provinces ON province = provinces.id' . $extraWhere
            . ' LIMIT ' . (int) (($curpage - 1) * $numItemsPerPage) . ',' . (int) ($numItemsPerPage));

        // find categories for each tool
        $this->findCategories($tools);
        return $tools;
    }



    private function adjustQuery($filter, &$extraJoins, &$extraWhere) {

        // City set via Filter
        if ($filter['city'] != '' || count($filter['provinces']) > 0) {
            $extraJoins .= ' INNER JOIN users ON users.id = tools.owner_id'
                . ' INNER JOIN cities ON cities.id = users.city';
        }

        // Name set via Filter
        if ($filter['name'] != '') {
            $extraWhere .= ' AND tools.name LIKE ' . $this->db->quote('%'.$filter['name'].'%', \PDO::PARAM_STR);
        }

        // City set via Filter
        if ($filter['city'] != '') {
            $extraWhere .= ' AND (cities.name LIKE ' . $this->db->quote('%'.$filter['city'].'%', \PDO::PARAM_STR)
                . ' OR cities.code LIKE ' . $this->db->quote('%'.$filter['city'].'%', \PDO::PARAM_STR) . ')';
        }

        // Date set via Filter
        if ($filter['start_date'] != '' && $filter['end_date'] != '') {
            $extraWhere .= ' AND tools.start_date <= ' . $this->db->quote($filter['start_date'], \PDO::PARAM_STR)
                .  'AND tools.end_date >= ' . $this->db->quote($filter['end_date'], \PDO::PARAM_STR);
        }
        elseif ($filter['start_date'] != '') {
            $extraWhere .= ' AND tools.start_date <= ' . $this->db->quote($filter['start_date'], \PDO::PARAM_STR)
                .  'AND tools.end_date >= ' . $this->db->quote($filter['start_date'], \PDO::PARAM_STR);
        }
        elseif ($filter['end_date'] != '') {
            $extraWhere .= ' AND tools.start_date <= ' . $this->db->quote($filter['end_date'], \PDO::PARAM_STR)
                .  'AND tools.end_date >= ' . $this->db->quote($filter['end_date'], \PDO::PARAM_STR);
        }

        // Max cost set via Filter
        if ($filter['max_cost'] !== null) {
            $extraWhere .= ' AND tools.price_day <= ' . $this->db->quote($filter['max_cost'], \PDO::PARAM_STR);
        }

        // Categories set via Filter
        if (count($filter['categories']) > 0) {
            $categories = '';
            foreach ($filter['categories'] as $categorie) {
                $categories .= $this->db->quote($categorie, \PDO::PARAM_STR);
                if ($categorie !== end($filter['categories'])) {
                    $categories .= ',';
                }
            }
            $extraJoins .= ' LEFT JOIN tools_has_categories ON tools.id = tools_id';
            $extraWhere .= ' AND categories_id IN ( '. $categories . ')';
        }

        // Provinces set via Filter
        if (count($filter['provinces']) > 0) {
            $provinces = '';
            foreach ($filter['provinces'] as $province) {
                $provinces .= $this->db->quote($province, \PDO::PARAM_STR);
                if ($province !== end($filter['provinces'])) {
                    $provinces .= ',';
                }
            }
            $extraWhere .= ' AND province IN ( '. $provinces . ')';
        }
    }



    private function findCategories(&$tools) {
        for ($i = 0; $i < count($tools); $i++) {
            $categories = $this->db->fetchAll('SELECT name FROM categories
                INNER JOIN tools_has_categories ON id = categories_id
                WHERE tools_id = ?', array($tools[$i]['id']));

            for ($j = 0; $j < count($categories); $j++) {
                $tools[$i]['categories'][$j] = $categories[$j]['name'];
            }
        }
    }
}