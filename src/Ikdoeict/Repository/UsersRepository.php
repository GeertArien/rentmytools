<?php
/**
 * Repository for users database
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Repository;

class UsersRepository extends \Knp\Repository {

    public function getTableName() {
        return 'users';
    }

    public function getProfile($userId) {
        return $this->db->fetchAssoc('SELECT users.id AS id, firstname, lastname, email, provinces.name AS province, cities.name AS city, code FROM users'
            . ' INNER JOIN cities ON city = cities.id'
            . ' INNER JOIN provinces ON provinces.id = cities.province'
            . ' WHERE users.id = ?', array($userId));
    }

    public function getProvinces() {
        $provinces = $this->db->fetchAll('SELECT * FROM provinces ORDER BY name');
        $provincesArray = [];
        for ($i = 0; $i < count($provinces); $i++) {
            $provincesArray[$provinces[$i]['id']] = $provinces[$i]['name'];
        }
        return $provincesArray;
    }

    public function findUser($email) {
        return $this->db->fetchAssoc('SELECT * from users WHERE users.email LIKE ?', array($email));
    }

    public function insert(array $data) {
        $this->db->insert($this->getTableName(), $data);
        return $this->db->lastInsertId();
    }
}