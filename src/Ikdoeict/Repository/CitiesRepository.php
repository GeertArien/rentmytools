<?php
/**
 * Repository for the cities database
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Repository;

class CitiesRepository extends \Knp\Repository {

    public function getTableName() {
        return 'cities';
    }

    public function countCities($name, $code) {
        return $this->db->fetchColumn('SELECT COUNT(*) FROM cities WHERE name LIKE ? AND code = ?', array($name, $code));
    }

    public function findId($name, $code) {
        return $this->db->fetchColumn('SELECT id from cities WHERE name LIKE ? && code = ?', array($name, $code));
    }
}