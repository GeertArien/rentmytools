<?php
/**
 * Controller for profile routes
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Profile implements ControllerProviderInterface {

    public function connect(Application $app) {

        // Create new ControllerCollection
        $controllers = $app['controllers_factory'];

        // View/manage profile
        $controllers
            ->get('/{profileId}/', array($this, 'details'))
            ->assert('profileId', '\d+')
            ->bind('profile.details');

        return $controllers;
    }


    public function details(Application $app, $profileId) {
        // get profile from database
        $profile = $app['db.users']->getProfile($profileId);

        // if profile is not valid redirect to homepage
        if ($profile === false) {
            return $app->redirect($app['url_generator']->generate('homepage'));
        }

        // get the tools for this user
        $tools = $app['db.tools']->getToolsByOwner($profileId);

        // Fetch images from profile image folder
        $file = $app['users.pictures.base_path'] . DIRECTORY_SEPARATOR . $profile['id'] . '.jpg';

        if (file_exists($file)) {
            $profile['picture'] = $app['users.pictures.base_url'] . '/' . $profile['id'] . '.jpg';
        }
        else {
            $profile['picture'] = $app['users.pictures.base_url'] . '/' . 'default.jpg';
        }

        // If this is the logged in user's profile page, show the management page
        if ($profileId == $app['session']->get('user')['id']) {
            // Fetch reservations for this user
            $reservations = $app['db.reservations']->findForUser($profile['id']);

            return $app['twig']->render('profile/manage.twig', array(
                'user' => $app['session']->get('user'),
                'profile' => $profile,
                'tools' => $tools,
                'reservations' => $reservations,
                'feedback' => $app['request']->get('feedback')
            ));
        }
        // Else show the 'view profile' page
        else {
            // Collect the tool images
            Tools::findImages($app, $tools);
            return $app['twig']->render('profile/view.twig', array(
                'user' => $app['session']->get('user'),
                'profile' => $profile,
                'tools' => $tools,
                'numitems' => count($tools)
            ));
        }
    }
}