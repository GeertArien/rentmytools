<?php
/**
 * Controller for reservation routes
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Tools implements ControllerProviderInterface {

    public function connect(Application $app) {

        // Create new ControllerCollection
        $controllers = $app['controllers_factory'];

        // Overview of tools
        $controllers
            ->match('/', array($this, 'overview'))
            ->method('GET|POST')
            ->bind('tools.overview');

        // View detail page of tool
        $controllers
            ->match('/{toolId}/', array($this, 'detail'))
            ->assert('toolId', '\d+')
            ->method('GET|POST')
            ->bind('tools.detail');

        // Insert new blogpost
        $controllers
            ->match('/new/', array($this, 'add'))
            ->method('GET|POST')
            ->before(array($this, 'checkLogin'))
            ->bind('tools.add');

        // Update a blogpost
        $controllers
            ->match('/{toolId}/edit/', array($this, 'edit'))
            ->assert('toolId', '\d+')
            ->method('GET|POST')
            ->before(array($this, 'checkLogin'))
            ->bind('tools.edit');

        // Delete a blogpost
        $controllers
            ->post('/{toolId}/delete/', array($this, 'delete'))
            ->assert('toolId', '\d+')
            ->before(array($this, 'checkLogin'))
            ->bind('tools.delete');

        return $controllers;
    }





    public function checkLogin(\Symfony\Component\HttpFoundation\Request $request, Application $app) {
        if (!$app['session']->get('user')) {
            return $app->redirect($app['url_generator']->generate('auth.login'));
        }
    }





    public function overview(Application $app) {

        // get categories and provinces from database
        $categories = $app['db.tools']->getCategories();
        $provinces = $app['db.users']->getProvinces();
        $cost = array(
            0 => 'Free',
            10 => '< 10 €',
            25 => '< 25 €',
            50 => '< 50 €',
            100 => '< 100 €'
        );

        $this->forceFilter($app);

        // Build the form
        $searchform = $app['form.factory']
            ->createNamed('searchform', 'form', $this->getFilter($app))
            ->add('name', 'text')
            ->add('city', 'text')
            ->add('start_date', 'date', array(
            'widget' => 'single_text',
            'input' => 'string',
            'format' => 'dd/MM/yyyy'
        ))
            ->add('end_date', 'date', array(
            'widget' => 'single_text',
            'input' => 'string',
            'format' => 'dd/MM/yyyy'
        ))
            ->add('max_cost', 'choice', array(
            'choices' => $cost,
            'empty_value' => 'Choose...',
        ))
            ->add('categories', 'choice', array(
            'choices' => $categories,
            'multiple' => true,
            'expanded' => true
        ))
            ->add('provinces', 'choice', array(
            'choices' => $provinces,
            'multiple' => true,
            'expanded' => true
        ));

        // form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $requests = $app['request']->request->all();

            // request comes from search form in nav bar
            if (isset($requests['globalsearch'])) {
                $filter = $this->getFilter($app);
                $filter['name'] = $requests['globalsearch']['name'];
                $searchform->get('name')->setData($requests['globalsearch']['name']);
                $this->setFilter($app, $filter);
            }
            // request comes from search form on overview page
            else if(isset($requests['searchform'])) {
                $searchform->handleRequest($app['request']);
                if ($searchform->isValid()) {
                    $this->setFilter($app, $searchform->getData());
                }
            }
        }
        // get paramter is set
        else if ('GET' == $app['request']->getMethod()) {
            $filterItem = $app['request']->query->get('filter');
            // filter item is set through get parameter
            if ($filterItem !== null) {
                $filter = $this->getFilter($app);
                // filter on categorie
                if (in_array($filterItem, $categories)) {
                    $categorie = array_search($filterItem, $categories);
                    $filter['categories'] = array($categorie);
                    $searchform->get('categories')->setData(array($categorie));
                }
                // filter on province
                else if (in_array($filterItem, $provinces)) {
                    $province = array_search($filterItem, $provinces);
                    $filter['provinces'] = array($province);
                    $searchform->get('provinces')->setData(array($province));
                }
                // filter on cost
                else if (array_key_exists($filterItem, $cost)) {
                    $filter['max_cost'] = $filterItem;
                    $searchform->get('max_cost')->setData($filterItem);
                }
                // filter on town/postal code
                else {
                    $filter['city'] = $filterItem;
                    $searchform->get('city')->setData($filterItem);
                }
                $this->setFilter($app, $filter);
            }
        }

        // count result set
        $numItems = $app['db.tools']->CountAllFiltered($this->getFilter($app));

        // found items
        if ($numItems > 0) {
            // get pagination if necessary
            $numItemsPerPage = $app['pagination']['numItemsPerPage'];
            $numPages = ceil($numItems / $numItemsPerPage);
            $curPage = max(1, (int) $app['request']->query->get('p'));
            $curPage = ($curPage > $numPages) ? $numPages : $curPage;

            if ($numPages > 1) {
                $pagination = new \Ikdoeict\Utility\Pagination($app, $curPage, $numPages);
            }

            // fetch result from database
            $tools = $app['db.tools']->findAllFiltered(
                $this->getFilter($app),
                $curPage,
                $numItemsPerPage
            );
            // find image for each tool
            Tools::findImages($app, $tools);
        }

        return $app['twig']->render('tools/overview.twig', array(
            'user' => $app['session']->get('user'),
            'tools' => isset($tools) ? $tools : null,
            'pagination' => isset($pagination) ? $pagination->getHtml() : null,
            'numitems' => $numItems,
            'searchform' => $searchform->createView()
        ));
    }





    public function detail(Application $app, $toolId) {

        // fetch tool data from database
        $tool = $app['db.tools']->find($toolId);

        // tool doesn't exist
        if ($tool === false) {
            return $app->redirect($app['url_generator']->generate('tools.overview'));
        }

        // find similar tools
        $similarTools = $app['db.tools']->findSimilar($tool);
        // find image for each similar tool
        $this->findImages($app, $similarTools);
        // calculate the available days for reservation
        $tool['reservations'] = $app['db.reservations']->findByToolId($tool['id']);
        $period = \Ikdoeict\Utility\Helper::DateInterval($tool['start_date'], $tool['end_date']);
        $period = \Ikdoeict\Utility\Helper::excludeIntervals($tool['reservations'], $period);
        $ranges = \Ikdoeict\Utility\Helper::createRanges($period);

        // Fetch images from tool image folder
        $tool['images'] = array();
        $dir = $app['tools.images.base_path'] . DIRECTORY_SEPARATOR . $toolId;

        if (is_dir($dir)) {
            $di = new \DirectoryIterator($dir);
            foreach ($di as $file) {
                if ($file->getExtension() == 'jpg') {
                    $url = $app['tools.images.base_url'] . '/' . $toolId . '/' . $file;
                    $tool['images'][] = $url;
                }
            }
        }

        if (count($tool['images']) == 0) {
            $tool['images'][] = $app['tools.images.base_url'] . '/' . 'default.jpg';
        }

        // user logged in and not the owner of the tool: create reservation form
        if (isset($app['session']->get('user')['id']) && $app['session']->get('user')['id'] != $tool['owner_id']) {
            $reservationform = $app['form.factory']
                ->createNamed('reservationform', 'form')
                ->add('start_date', 'date', array(
                'widget' => 'single_text',
                'input' => 'string',
                'format' => 'dd/MM/yyyy',
                'constraints' => array(new Assert\NotBlank())
            ))
                ->add('end_date', 'date', array(
                'widget' => 'single_text',
                'input' => 'string',
                'format' => 'dd/MM/yyyy',
                'constraints' => array(new Assert\NotBlank())
            ))
                ->add('message', 'textarea', array(
                'max_length' => 255,
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Length(array(
                        'min'        => 5,
                        'max'        => 255,
                        'minMessage' => 'Message must be atleast 5 characters long',
                        'maxMessage' => 'Message can be only 255 characters long',
                    )))
            ));

            // reservation form was submitted
            if ('POST' == $app['request']->getMethod()) {
                $reservationform->bind($app['request']);

                $data = $reservationform->getData();

                // date validation
                if ($data['start_date'] != '' && $data['end_date'] != '') {
                    if (!($data['start_date'] <= $data['end_date'])) {
                        $reservationform->get('start_date')->addError(new \Symfony\Component\Form\FormError('Invalid time frame.'));
                        $reservationform->get('end_date')->addError(new \Symfony\Component\Form\FormError(''));
                    }
                    else {
                        $newPeriod = \Ikdoeict\Utility\Helper::DateInterval($data['start_date'], $data['end_date']);
                        if (!\Ikdoeict\Utility\Helper::ContainsPeriod($newPeriod, $period)) {
                            $reservationform->get('start_date')->addError(new \Symfony\Component\Form\FormError('Tool is not available for this time frame.'));
                            $reservationform->get('end_date')->addError(new \Symfony\Component\Form\FormError(''));
                        }
                    }
                }

                // Form is valid
                if ($reservationform->isValid()) {
                    $user = $app['session']->get('user');

                    $data['tool_id'] = $tool['id'];
                    $data['rentee_id'] = $user['id'];
                    $data['status'] = 'pending';

                    // insert data is successful
                    if ($app['db.reservations']->insert($data) > 0) {

                        // set mail template
                        $template = $app['twig']->loadTemplate('mails/request_rentee.twig');

                        // To send HTML mail, the Content-type header must be set
                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                        // Additional headers
                        $headers .= 'From: RentMyTools <noreply@rentmytools.com>' . "\r\n";

                        // send mail to rentee
                        mail($user['email'],$template->renderBlock('subject',array(
                            'toolname' => $tool['name']
                        )), $template->renderBlock('body', array(
                            'firstname' => $user['firstname'],
                            'tool' => $tool,
                            'start_date' => $data['start_date'],
                            'end_date' => $data['end_date']
                        )), $headers);

                        // set mail template
                        $template = $app['twig']->loadTemplate('mails/request_renter.twig');

                        // fetch tool owner information from database
                        $owner = $app['db.users']->getProfile($tool['owner_id']);

                        // send mail to new owner
                        mail($owner['email'],$template->renderBlock('subject',array(
                            'toolname' => $tool['name']
                        )), $template->renderBlock('body', array(
                            'rentee' => $user,
                            'tool' => $tool,
                            'owner' => $owner,
                            'start_date' => $data['start_date'],
                            'end_date' => $data['end_date'],
                            'message' => $data['message']
                        )), $headers);

                        // redirect to reservation completed page
                        return $app['twig']->render('reservation/complete.twig', array(
                            'user' => $app['session']->get('user'),
                            'tool' => $tool,
                            'similartools' => $similarTools
                        ));
                    }
                }
            }
        }
        return $app['twig']->render('tools/details.twig', array(
            'user' => $app['session']->get('user'),
            'tool' => $tool,
            'similartools' => $similarTools,
            'reservationform' => isset($reservationform) ? $reservationform->createView() : null,
            'period' => isset($period) ? $period : null,
            'ranges' => $ranges
        ));
    }





    public function add(Application $app) {

        // get categories from database
        $categories = $app['db.tools']->getCategories();

        // Build the form
        $addform = $app['form.factory']
            ->createNamed('addform', 'form')
            ->add('name', 'text', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
        ))
            ->add('description', 'textarea', array(
            'constraints' => array(new Assert\NotBlank())
        ))
            ->add('start_date', 'date', array(
            'widget' => 'single_text',
            'input' => 'string',
            'format' => 'dd/MM/yyyy',
            'constraints' => array(new Assert\NotBlank())
        ))
            ->add('end_date', 'date', array(
            'widget' => 'single_text',
            'input' => 'string',
            'format' => 'dd/MM/yyyy',
            'constraints' => array(new Assert\NotBlank())
        ))
            ->add('price_day', 'money', array(
            'constraints' => new Assert\Range(array(
                'min'        => 0,
                'max'        => 1000
            ))))
            ->add('categories', 'choice', array(
            'choices' => $categories,
            'multiple' => true,
            'expanded' => true,
            'constraints' => array(new Assert\Count(array(
                'min' => 1,
                'max' => 3,
                'minMessage' => 'You must select at least one category',
                'maxMessage' => 'You cannot select more than 3 categories'
            )))))
            ->add('images', 'file', array(
            'constraints' => array(
                new Assert\Count(array(
                    'max' => 3
                )),
                new Assert\Collection(array(
                    'fields' => array(
                        0  => new Assert\Optional(new Assert\File(array(
                            'maxSize' => '1024k'
                        ))),
                        1 => new Assert\Optional(new Assert\File(array(
                            'maxSize' => '1024k'
                        ))),
                        2 => new Assert\Optional(new Assert\File(array(
                            'maxSize' => '1024k'
                        )))
                    ))))));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $addform->bind($app['request']);
            $files = $app['request']->files->get($addform->getname());

            // validate image file extensions && size
            if (isset($files['images']) && $files['images'][0] != null) {
                foreach ($files['images'] as $file) {
                    $fileInfo = pathinfo($file->getClientOriginalName());
                    if (strtolower($fileInfo['extension']) != 'jpg' && strtolower($fileInfo['extension']) != 'jpeg') {
                        $addform->get('images')->addError(new \Symfony\Component\Form\FormError(''));
                        break;
                    }
                }
            }

            $data = $addform->getData();

            // date validation
            if (isset($data['start_date']) && isset($data['end_date']) && !($data['start_date'] < $data['end_date'])) {
                $addform->get('start_date')->addError(new \Symfony\Component\Form\FormError('Invalid time frame.'));
                $addform->get('end_date')->addError(new \Symfony\Component\Form\FormError(''));
            }

            // Form is valid
            if ($addform->isValid()) {

                unset($data['images']);
                $data['owner_id'] = $app['session']->get('user')['id'];

                // insert new tool into database
                $id = $app['db.tools']->insert($data);

                // insert was successful
                if ($id != 0) {
                    // Move files to specified folder
                    if (isset($files['images']) && $files['images'][0] != null) {
                        foreach ($files['images'] as $file) {
                            $fileInfo = pathinfo($file->getClientOriginalName());
                            $file->move($app['tools.images.base_path'] . DIRECTORY_SEPARATOR . $id,
                                $fileInfo['filename'] . '-' . date("Y-m-d-His"). '.jpg');
                        }
                    }
                    // Redirect to overview
                    return $app->redirect($app['url_generator']->generate('profile.details', array(
                            'profileId' => $app['session']->get('user')['id'])
                    ) . '?feedback=added');
                }
            }
        }
        return $app['twig']->render('tools/add.twig', array(
            'user' => $app['session']->get('user'),
            'addform' => $addform->createView()
        ));
    }





    public function edit(Application $app, $toolId) {

        // get tool data from database
        $tool = $app['db.tools']->findForOwner($toolId, $app['session']->get('user')['id']);

        // tool doesn't exist for this user
        if ($tool === false) {
            return $app->redirect($app['url_generator']->generate('profile.details', array(
                'profileId' => $app['session']->get('user')['id']
            )));
        }

        $images = [];
        $deleteImages = [];
        $dir = $app['tools.images.base_path'] . DIRECTORY_SEPARATOR . $toolId;

        // Fetch images from tool image folder
        if (is_dir($dir)) {
            $di = new \DirectoryIterator($dir);
            foreach ($di as $file) {
                if ($file->getExtension() == 'jpg') {
                    $url = $app['tools.images.base_url'] . '/' . $toolId . '/' . $file;
                    $images[] = array(
                        'url' => $url,
                        'title' => $file->getFileName()
                    );
                    $deleteImages[$file->getFileName()] = $url;
                }
            }
        }

        // get categories from database
        $categories = $app['db.tools']->getCategories();

        // Build the form
        $editform = $app['form.factory']
            ->createNamed('editform', 'form', $tool)
            ->add('name', 'text', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
        ))
            ->add('description', 'textarea', array(
            'constraints' => array(new Assert\NotBlank())
        ))
            ->add('start_date', 'date', array(
            'widget' => 'single_text',
            'input' => 'string',
            'format' => 'dd/MM/yyyy',
            'constraints' => array(new Assert\NotBlank())
        ))
            ->add('end_date', 'date', array(
            'widget' => 'single_text',
            'input' => 'string',
            'format' => 'dd/MM/yyyy',
            'constraints' => array(new Assert\NotBlank())
        ))
            ->add('price_day', 'money', array(
            'constraints' => new Assert\Range(array(
                'min'        => 0,
                'max'        => 1000
            ))))
            ->add('categories', 'choice', array(
            'choices' => $categories,
            'multiple' => true,
            'expanded' => true,
            'constraints' => array(new Assert\Count(array(
                'min' => 1,
                'max' => 3,
                'minMessage' => 'You must select at least one category',
                'maxMessage' => 'You cannot select more than 3 categories'
            )))))
            ->add('addImages', 'file', array(
            'constraints' => array(
                new Assert\Count(array(
                    'max' => 3
                )),
                new Assert\Collection(array(
                    'fields' => array(
                        0  => new Assert\Optional(new Assert\File(array(
                            'maxSize' => '1024k'
                        ))),
                        1 => new Assert\Optional(new Assert\File(array(
                            'maxSize' => '1024k'
                        ))),
                        2 => new Assert\Optional(new Assert\File(array(
                            'maxSize' => '1024k'
                        )))))))))
            ->add('deleteImages', 'choice', array(
            'choices' => $deleteImages,
            'multiple' => true,
            'expanded' => true
        ));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $editform->bind($app['request']);
            $files = $app['request']->files->get($editform->getname());

            // validate image file extensions
            if (isset($files['addImages']) && $files['addImages'][0] != null) {
                foreach ($files['addImages'] as $file) {
                    $fileInfo = pathinfo($file->getClientOriginalName());
                    if (strtolower($fileInfo['extension']) != 'jpg' && strtolower($fileInfo['extension']) != 'jpeg') {
                        $editform->get('addImages')->addError(new \Symfony\Component\Form\FormError('Only jpg images are allowed.'));
                        break;
                    }
                }
            }

            $data = $editform->getData();

            // date validation
            if ($data['start_date'] && $data['end_date'] && !($data['start_date'] < $data['end_date'])) {
                $editform->get('start_date')->addError(new \Symfony\Component\Form\FormError('Invalid time frame.'));
                $editform->get('end_date')->addError(new \Symfony\Component\Form\FormError(''));
            }

            // only 3 images allowed for one tool
            if ($files['addImages'][0] != null && count($images) - count($data['deleteImages']) + count($files['addImages']) > 3) {
                $editform->get('addImages')->addError(new \Symfony\Component\Form\FormError(''));
            }

            // Form is valid
            if ($editform->isValid()) {

                // Move files to blogpost image folder
                if (isset($files['addImages']) && $files['addImages'][0] != null) {
                    foreach ($files['addImages'] as $file) {
                        $fileInfo = pathinfo($file->getClientOriginalName());
                        $file->move($app['tools.images.base_path'] . DIRECTORY_SEPARATOR . $tool['id'],
                            $fileInfo['filename'] . '-' . date("Y-m-d-His"). '.jpg');
                    }
                }

                // Delete selected images
                if ($data['deleteImages']) {
                    foreach($data['deleteImages'] as $image) {
                        unlink($app['tools.images.base_path'] . DIRECTORY_SEPARATOR . $tool['id'] . DIRECTORY_SEPARATOR . $image);
                    }
                }

                unset($data['addImages']);
                unset($data['deleteImages']);

                // Update data in DB
                $app['db.tools']->update($data, array('id' => $tool['id']));

                // Redirect to overview
                return $app->redirect($app['url_generator']->generate('profile.details', array(
                        'profileId' => $app['session']->get('user')['id'])
                ) . '?feedback=edited');
            }
        }
        return $app['twig']->render('tools/edit.twig', array(
            'user' => $app['session']->get('user'),
            'toolId' => $tool['id'],
            'toolName' => $tool['name'],
            'editform' => $editform->createView(),
            'images' => $images
        ));
    }






    public function delete(Application $app, $toolId) {
        // Fetch tool with given $toolId and logged in user Id
        $tool = $app['db.tools']->findForOwner($toolId, $app['session']->get('user')['id']);

        // Redirect to overview if it does not exist
        if ($tool === false) {
            return $app->redirect($app['url_generator']->generate('profile.details', array('profileId' => $app['session']->get('user')['id'])));
        }

        // Delete the blogpost
        $app['db.tools']->delete(array('id' => $tool['id']));

        // delete all images for this tool
        $this->deleteDirectory($app['tools.images.base_path'] . DIRECTORY_SEPARATOR . $toolId);

        // Redirect to tool management with feedback
        return $app->redirect($app['url_generator']->generate('profile.details', array(
                'profileId' => $app['session']->get('user')['id'])
        ) . '?feedback=deleted');
    }





    private function forceFilter(Application $app) {
        // force a filter for the searchform if user didn't request a next page
        if (!$app['request']->query->get('p')) {
            $app['session']->set('searchform', array(
                'name' => null,
                'city' => null,
                'start_date' => '',
                'end_date' => '',
                'max_cost' => null,
                'categories' => array(),
                'provinces' => array()
            ));
        }
    }






    private function getFilter(Application $app) {
        return $app['session']->get('searchform');
    }






    private function setFilter(Application $app, $filter) {
        $app['session']->set('searchform', $filter);
    }






    public static function findImages($app, &$tools) {
        for ($i = 0; $i < count($tools); $i++) {
            $dir = $app['tools.images.base_path'] . DIRECTORY_SEPARATOR . $tools[$i]['id'];

            // Fetch images from tool image folder
            if (is_dir($dir)) {
                $di = new \DirectoryIterator($dir);
                foreach ($di as $file) {
                    if ($file->getExtension() == 'jpg') {
                        $url = $app['tools.images.base_url'] . '/' . $tools[$i]['id'] . '/' . $file;
                        $tools[$i]['image'] = $url;
                        break;
                    }
                }
            }
            if (!isset($tools[$i]['image'])) {
                $tools[$i]['image'] = $app['tools.images.base_url'] . '/' . 'default.jpg';
            }
        }
    }





    private function deleteDirectory($dir) {
        // if directory exist delete directory with all files in it
        if (is_dir($dir)) {
            if (substr($dir, strlen($dir) - 1, 1) != '/') {
                $dir .= '/';
            }
            $files = glob($dir . '*', GLOB_MARK);
            foreach ($files as $file) {
                unlink($file);
            }
            rmdir($dir);
        }
    }
}