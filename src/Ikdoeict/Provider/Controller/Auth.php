<?php
/**
 * Controller for authorization routes
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Auth implements ControllerProviderInterface {


    public function connect(Application $app) {
        // Create new ControllerCollection
        $controllers = $app['controllers_factory'];

        // Login
        $controllers
            ->match('/login/', array($this, 'login'))
            ->method('GET|POST')
            ->bind('auth.login');

        // Logout
        $controllers
            ->get('/logout/', array($this, 'logout'))
            ->bind('auth.logout');

        // Register
        $controllers
            ->match('/register/', array($this, 'register'))
            ->method('GET|POST')
            ->bind('auth.register');

        // Redirect to login by default
        $controllers->get('/', function(Application $app) {
            return $app->redirect($app['url_generator']->generate('auth.login'));
        });

        return $controllers;

    }



    public function register(Application $app) {
        // Already logged in
        if ($app['session']->get('user')) {
            return $app->redirect($app['url_generator']->generate('homepage'));
        }

        // Build the form
        $registerform = $app['form.factory']
            ->createNamed('registerform', 'form')
            ->add('firstname', 'text', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
        ))
            ->add('lastname', 'text', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
        ))
            ->add('address', 'text', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
        ))
            ->add('city', 'text', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
        ))
            ->add('postal', 'text', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Range(array(
                'min'        => 0612,
                'max'        => 9992,
                'invalidMessage' => 'Please enter a valid postal code'
        )))))
            ->add('email', 'email', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Email())
        ))
            ->add('password1', 'password', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
        ))
            ->add('password2', 'password', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))))
            ->add('picture', 'file', array(
            'constraints' => new Assert\File(array(
                            'maxSize' => '1024k'
        ))));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $registerform->bind($app['request']);
            $files = $app['request']->files->get($registerform->getname());

            // validate image file extensions
            if (isset($files['picture']) && $files['picture'] != null) {
                $fileInfo = pathinfo($files['picture']->getClientOriginalName());
                if (strtolower($fileInfo['extension']) != 'jpg' && strtolower($fileInfo['extension']) != 'jpeg') {
                    $registerform->get('picture')->addError(new \Symfony\Component\Form\FormError('Invalid file type'));
                }
            }

            $data = $registerform->getData();

            // validate email address not being duplicate
            if ($app['db.users']->findUser($data['email'])) {
                $registerform->get('email')->addError(new \Symfony\Component\Form\FormError('There is already a user registered with this email address.'));
            }

            // validate passwords being equal
            if ($data['password1'] != null && $data['password2'] != null && $data['password1'] != $data['password2']) {
                $registerform->get('password2')->addError(new \Symfony\Component\Form\FormError('Password mismatch'));
            }

            // validate city
            if ($data['city'] != null && $data['postal'] != null && $app['db.cities']->countCities($data['city'], $data['postal']) != 1) {
                $registerform->get('city')->addError(new \Symfony\Component\Form\FormError('City and postal code don\'t match'));
                $registerform->get('postal')->addError(new \Symfony\Component\Form\FormError(''));
            }

            // Form is valid
            if ($registerform->isValid()) {

                $newuser['firstname'] = strtolower($data['firstname']);
                $newuser['lastname'] = strtolower($data['lastname']);
                $newuser['email'] = strtolower($data['email']);
                $newuser['address'] = strtolower($data['address']);
                $newuser['city'] = $app['db.cities']->findId($data['city'], $data['postal']);
                $newuser['password'] = crypt($data['password1']);

                // insert data into database
                $id = $app['db.users']->insert($newuser);

                // insert was successful
                if ($id !== 0) {
                    // move uploaded picture to the picture folder
                    if (isset($files['picture']) && $files['picture'] != null) {
                        $files['picture']->move($app['users.pictures.base_path'] . DIRECTORY_SEPARATOR, $id . '.jpg');
                    }

                    // Unset user password from record so that no-one can read the (encrypted) password from the session
                    unset($newuser['password']);
                    $newuser['id'] = $id;

                    // Store user in session
                    $app['session']->set('user', $newuser);

                    $template = $app['twig']->loadTemplate('mails/registered.twig');

                    // To send HTML mail, the Content-type header must be set
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    // Additional headers
                    $headers .= 'From: RentMyTools <noreply@rentmytools.com>' . "\r\n";

                    // send mail to new user
                    mail($newuser['email'],$template->renderBlock('subject',array()), $message = $template->renderBlock('body', array(
                        'user' => $newuser
                    )), $headers);

                    // Redirect to overview
                    return $app->redirect($app['url_generator']->generate('profile.details', array(
                            'profileId' => $app['session']->get('user')['id'])
                    ) . '?feedback=registered');
                }
            }
        }

        return $app['twig']->render('auth/register.twig', array(
            'user' => null,
            'registerform' => $registerform->createView()
        ));
    }


    public function login(Application $app) {

        // Already logged in
        if ($app['session']->get('user')) {
            return $app->redirect($app['url_generator']->generate('homepage'));
        }

        // Create Form
        $loginform = $app['form.factory']->createNamed('loginform')
            ->add('email', 'email', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Email())
        ))
            ->add('password', 'password', array(
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
        ));

        // Form was submitted: process it
        if ('POST' == $app['request']->getMethod()) {
            $loginform->bind($app['request']);

            // Form is valid
            if ($loginform->isValid()) {

                $data = $loginform->getData();
                $user = $app['db.users']->findUser($data['email']);

                // Password checks out
                if (crypt($data['password'], $user['password']) === $user['password'] ) {

                    // Unset user password from record so that no-one can read the (encrypted) password from the session
                    unset($user['password']);

                    // Store user in session
                    $app['session']->set('user', $user);

                    // Redirect to profile page
                    return $app->redirect($app['url_generator']->generate('profile.details', array(
                            'profileId' => $app['session']->get('user')['id'])
                    ));
                }

                // Password does not check out: add an error to the form
                else {
                    $loginform->get('password')->addError(new \Symfony\Component\Form\FormError('Invalid credentials'));
                }
            }
        }
        return $app['twig']->render('auth/login.twig', array(
            'user' => null,
            'loginform' => $loginform->createView()
        ));
    }


    public function logout(Application $app) {
        $app['session']->remove('user');
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

}