<?php
/**
 * Controller for reservation routes
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;


class Reservations implements ControllerProviderInterface {

    public function connect(Application $app) {

        // Create new ControllerCollection
        $controllers = $app['controllers_factory'];

        // accept a reservation
        $controllers
            ->post('/{reservationId}/accept/', array($this, 'accept'))
            ->assert('reservationId', '\d+')
            ->before(array($this, 'checkLogin'))
            ->bind('reservations.accept');

        // decline a reservation
        $controllers
            ->post('/{reservationId}/decline/', array($this, 'decline'))
            ->assert('reservationId', '\d+')
            ->before(array($this, 'checkLogin'))
            ->bind('reservations.decline');

        return $controllers;
    }


    public function checkLogin(\Symfony\Component\HttpFoundation\Request $request, Application $app) {
        if (!$app['session']->get('user')) {
            return $app->redirect($app['url_generator']->generate('auth.login'));
        }
    }


    public function accept(Application $app, $reservationId) {

        // Fetch reservation with given $reservationId and logged in user Id
        $reservation = $app['db.reservations']->findForOwner($reservationId, $app['session']->get('user')['id']);

        // Redirect to overview if it does not exist
        if ($reservation === false) {
            return $app->redirect($app['url_generator']->generate('profile.details', array('profileId' => $app['session']->get('user')['id'])));
        }

        // set the status of the route to accepted
        $affectedRows = $app['db.reservations']->update(array('status' => 'accepted'), array('id' => $reservationId));

        // the update was successful
        if ($affectedRows > 0) {

            $user = $app['session']->get('user');
            // fetch reservation information from database
            $info = $app['db.reservations']->findForAccept($reservationId);
            // fetch owner city information from database
            $ownerCity = $app['db.cities']->find($user['city']);
            $user['city'] = $ownerCity['name'];
            $user['code'] = $ownerCity['code'];

            // set mail template
            $template = $app['twig']->loadTemplate('mails/accept_rentee.twig');

            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            // Additional headers
            $headers .= 'From: RentMyTools <noreply@rentmytools.com>' . "\r\n";

            // send mail to rentee
            mail($info['rentee_email'],$template->renderBlock('subject',array(
                'toolname' => $info['tool_name']
            )),$template->renderBlock('body', array(
                'info' => $info,
                'owner' => $user
            )), $headers);

            // set mail template
            $template = $app['twig']->loadTemplate('mails/accept_owner.twig');

            // send mail to owner
            mail($user['email'],$template->renderBlock('subject',array(
                'toolname' => $info['tool_name']
            )),$template->renderBlock('body', array(
                'info' => $info,
                'owner' => $user
            )), $headers);

            // redirect to profile management with feedback
            return $app->redirect($app['url_generator']->generate('profile.details', array(
                    'profileId' => $app['session']->get('user')['id'])
            ) . '?feedback=accepted');
        }
    }


    public function decline(Application $app, $reservationId) {

        // Fetch reservation with given $reservationId and logged in user Id
        $reservation = $app['db.reservations']->findForOwner($reservationId, $app['session']->get('user')['id']);

        // Redirect to overview if it does not exist
        if ($reservation === false) {
            return $app->redirect($app['url_generator']->generate('profile.details', array('profileId' => $app['session']->get('user')['id'])));
        }

        // set status of the route to declined
        $affectedRows = $app['db.reservations']->update(array('status' => 'declined'), array('id' => $reservationId));

        // database update was successful
        if ($affectedRows > 0) {

            // fetch reservation information from database
            $info = $app['db.reservations']->findForDecline($reservationId);

            // set mail template
            $template = $app['twig']->loadTemplate('mails/decline_rentee.twig');

            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            // Additional headers
            $headers .= 'From: RentMyTools <noreply@rentmytools.com>' . "\r\n";

            // send mail to rentee
            mail($info['email'],$template->renderBlock('subject',array(
                'toolname' => $info['tool_name']
            )),$template->renderBlock('body', array(
                'info' => $info
            )), $headers);

            // redirect to profile management with feedback
            return $app->redirect($app['url_generator']->generate('profile.details', array(
                    'profileId' => $app['session']->get('user')['id'])
            ) . '?feedback=declined');
        }
    }
 }