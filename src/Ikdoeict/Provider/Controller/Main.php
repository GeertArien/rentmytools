<?php
/**
 * Main route controller
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

namespace Ikdoeict\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Main implements ControllerProviderInterface {

    public function connect(Application $app) {

        // Create new ControllerCollection
        $controllers = $app['controllers_factory'];

        // View homepage
        $controllers
            ->get('/', array($this, 'home'))
            ->bind('homepage');

        // View sitemap
        $controllers
            ->get('/sitemap/', array($this, 'sitemap'))
            ->bind('sitemap');


        // Mount our Controllers
        $app->mount('/auth/', new Auth());
        $app->mount('/profile/', new Profile());
        $app->mount('/tools/', new Tools());
        $app->mount('/reservations/', new Reservations());

        return $controllers;
    }


    public function home(Application $app) {
        // collect categories and provinces from database
        $categories = $app['db.tools']->getCategories();
        $provinces = $app['db.users']->getProvinces();
        $cost = array(
            0 => 'Free',
            10 => '< 10 €',
            25 => '< 25 €',
            50 => '< 50 €',
            100 => '< 100 €'
        );

        // Build the search form
        $searchform = $app['form.factory']
            ->createNamed('searchform', 'form')
            ->add('name', 'text')
            ->add('city', 'text');

        return $app['twig']->render('home.twig', array(
            'user' => $app['session']->get('user'),
            'categories' => $categories,
            'provinces' => $provinces,
            'cost' => $cost,
            'searchform' => $searchform->createView()
        ));
    }


    public function sitemap(Application $app) {
        // collect categories and provinces from database
        $categories = $app['db.tools']->getCategories();
        $provinces = $app['db.users']->getProvinces();
        $cost = array(
            0 => 'Free',
            10 => '< 10 €',
            25 => '< 25 €',
            50 => '< 50 €',
            100 => '< 100 €'
        );

        return $app['twig']->render('sitemap.twig', array(
            'user' => $app['session']->get('user'),
            'categories' => $categories,
            'provinces' => $provinces,
            'cost' => $cost
        ));
    }

}