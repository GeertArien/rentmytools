<?php
/**
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

// Bootstrap
require __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

// catch errors
$app->error(function (\Exception $e, $code) use($app) {
	if ($code == 404) {
		return $app['twig']->render('errors/error.twig', array(
            'user' => $app['session']->get('user'),
            'error'=> 'I\'m sorry, we couldn\'t find the file you were looking for.'
        ));
	} else {
        return $app['twig']->render('errors/error.twig', array(
            'user' => $app['session']->get('user'),
            'error'=> 'I\'m sorry, we encountered an error.'
        ));
	}
});


// Mount our Controllers
$app->mount('/', new Ikdoeict\Provider\Controller\Main());


