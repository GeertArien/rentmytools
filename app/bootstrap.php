<?php
/**
 *
 * Solution to project 1, Webscripting 2 (2014)
 * @author Geert Ariën <geert.arien@kahosl.be>
 *
 */

// Require Composer Autoloader
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

// Create new Silex App
$app = new Silex\Application();

// App Configuration
$app['debug'] = true;

// Load Configuration into container
$app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__ . '/config.yml'));

// Use Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ .  DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'views'
));

// Use Doctrine — @note: Be sure to install Doctrine via Composer first!
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_mysql',
        'dbname' => $app['db.options']['dbname'],
        'user' => $app['db.options']['user'],
        'password' => $app['db.options']['password'],
        'host' => $app['db.options']['host'],
        'charset' => 'utf8',
        'driverOptions' => array(
            1002 => 'SET NAMES utf8'
        )
    )
));

// Use Repository Service Provider
$app->register(new Knp\Provider\RepositoryServiceProvider(), array(
    'repository.repositories' => array(
        'db.users' => 'Ikdoeict\\Repository\\UsersRepository',
        'db.cities' => 'Ikdoeict\\Repository\\CitiesRepository',
        'db.tools' => 'Ikdoeict\\Repository\\ToolsRepository',
        'db.reservations' => 'Ikdoeict\\Repository\\ReservationsRepository'
    )
));

// Use UrlGeneratorServiceProvider
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Use SessionGeneratorServiceProvider
$app->register(new Silex\Provider\SessionServiceProvider());

// Use Validator Service Provider
$app->register(new Silex\Provider\ValidatorServiceProvider());

// Use Form Service Provider
$app->register(new Silex\Provider\FormServiceProvider());

// Use Translation Service Provider because without it our form won't work
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

